class NanniesController < ApplicationController
  
  def index
      @nannies = Nanny.paginate(page: params[:page], :per_page => 10)
  end
  
  
  def destroy
    Nanny.find(params[:id]).destroy
    flash[:success] = "This Nanny has been deleted"
    redirect_to nannies_url
  end
  
  
  def show
    @nanny = Nanny.find(params[:id])
  end
  
  def new
     @nanny = Nanny.new
  end
  
  def create
    @nanny = Nanny.new(nanny_params)
    if @nanny.save
      flash[:success] = "Welcome Nanny!"
     redirect_to @nanny
    else
      render 'new'
    end
  end
  
    def edit
    @nanny = Nanny.find(params[:id])
    end
  
   private

    def nanny_params
      params.require(:nanny).permit(:name, :email, :password,
                                   :password_confirmation)
    end
  
end
