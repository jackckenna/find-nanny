class AddPasswordDigestToNannies < ActiveRecord::Migration
  def change
    add_column :nannies, :password_digest, :string
  end
end
