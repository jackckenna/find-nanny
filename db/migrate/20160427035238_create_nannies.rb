class CreateNannies < ActiveRecord::Migration
  def change
    create_table :nannies do |t|
      t.string :name
      t.string :email

      t.timestamps null: false
    end
  end
end
